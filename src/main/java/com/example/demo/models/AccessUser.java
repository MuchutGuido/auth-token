package com.example.demo.models;

import java.io.Serializable;
import java.time.LocalDate;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "AccessUsers")
public class AccessUser implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private String id;
	
	private String token;
	
	@DBRef
	private User user;
	
	private LocalDate fecha_acceso;
	
	public AccessUser() {
		
	}
	
	public AccessUser(String token, User user) {
		this.token = token;
		this.user = user;
		this.fecha_acceso = LocalDate.now();
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public LocalDate getFecha_acceso() {
		return fecha_acceso;
	}

	public void setFecha_acceso(LocalDate fecha_acceso) {
		this.fecha_acceso = fecha_acceso;
	}
	
}
