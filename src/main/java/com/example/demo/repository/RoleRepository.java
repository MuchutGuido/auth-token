package com.example.demo.repository;

import java.io.Serializable;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.models.ERole;
import com.example.demo.models.Role;

@Repository
public interface RoleRepository extends MongoRepository <Role, Serializable> {

	Optional<Role> findByName(ERole name);
}
