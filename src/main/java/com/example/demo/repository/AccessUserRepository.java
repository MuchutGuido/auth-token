package com.example.demo.repository;

import java.io.Serializable;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.demo.models.AccessUser;

public interface AccessUserRepository extends MongoRepository<AccessUser, Serializable> {

}
