package com.example.demo.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.models.Role;
import com.example.demo.models.User;
import com.example.demo.repository.RoleRepository;
import com.example.demo.repository.UserRepository;

@RestController
@RequestMapping("/api/roles")
public class UsersController {

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	RoleRepository roleRepository;
	
	@GetMapping()
	public List<Role> listarRoles() {
		return roleRepository.findAll();
	}
	
//	@Secured("ROLE_ADMIN")
//	@PreAuthorize("hasRole('ROLE_ADMIN') AND hasRole('ROLE_MODERATOR')")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@GetMapping("users")
	public List<User> listarUsers() {
		return userRepository.findAll();
	}
}
